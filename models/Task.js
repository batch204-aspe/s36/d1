// Create Schema, model and export the file


const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Use "module.exports" is a way for Node JS to treat this value as "package" that can be use by other files
module.exports = mongoose.model("Task", taskSchema);