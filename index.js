// Set-up dependencies
const express = require("express");
const mongoose = require ("mongoose");

// Declaring the app server
const app = express();
const port = 3000;

// this allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

app.use(express.json());

// Database Connection
mongoose.connect("mongodb+srv://admin:admin123@batch204-aspemarkjoseph.iwwi5jg.mongodb.net/B204-to-dos?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Set notification for connection success or failure connection of database
let db = mongoose.connection;
	// If a connection error occured
	db.on("error", console.error.bind(console, "Connection Error"));
	// If the connection is successfuls
	db.once("open", () => console.log("We're Connected to the cloud database."));


// add the task ROute by using the app
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now Listening to port ${port}`));