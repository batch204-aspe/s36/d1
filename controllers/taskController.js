// Controllers contain the functions and business logic of our Express JS application

// To import the Models
// to use it we need to "Store the path of model file to a variable"
// Allow us to use the contents of the "Task.js" file in the models folder
const Task = require("../models/Task");


// Controller function for getting all the tasks

// Retrieving
module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result
	});

}

// Creating a New Documents
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	});

	//to save we need to use .save() method to the database
	return newTask.save().then((task, error) => {

		if (error) {
			console.log(error)
			return false
		} else {
			return task
		}

	});
}


// Deleting/Removing a Document
module.exports.deleteTask = (taskId) => {

		return Task.findByIdAndRemove(taskId).then((removedTask, err) => {

			if (err) {
				console.log(err)
				return false
			} else {
				return removedTask
			}
		});
}

// Updating a Document
module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, error) => {

		if (error) {
				console.log(error)
				return false
		}

			result.name = newContent.name;

			return result.save().then((updateTask, saveErr) => {

				if (saveErr) {
						console.log(saveErr)
						return false
				} else {
						return updateTask
				}

			});

	});
}


/********** Activity Start **********/
	/*
		Instructions:
			- Create a ROUTE for "getting a Specific task".
			- Create a CONTROLLER function for "Retrieving a Specific task".
			- Return the result back to the client/postman.
			- Process a GET request at the "/tasks/:id" endpoint route using postman to "Get a specific task".
			- Create a route for "Changing the status to COMPLETE".
			- Create a CONTROLLER function for "changing the status of a task to complete".
			- Return the result back to the client/postman.
			Process a PUT request at the "/tasks/:id/complete" endpoint route using postman to "update a task".
	*/
		// Updating Status from Pending to Complete
		module.exports.updateStatusTask = (taskId, newContentStatus) => {

			return Task.findById(taskId).then((result, error) => {

				if (error) {
						console.log(error)
						return false
				}

					result.status = newContentStatus.status;

					return result.save().then((updateTask, saveErr) => {

						if (saveErr) {
								console.log(saveErr)
								return false
						} else {
								return updateTask
						}

					});

			});
		}


		// Retrieve a Specific Task
		module.exports.getTask = (taskId) => {

			return Task.findById(taskId).then((getTask, error) => {

				if (error) {
					console.log(error)
					return false
				} else {
					return getTask
				}
			});
		}
/********** Activity End **********/