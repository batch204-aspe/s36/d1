/*
	- Contains all the endpoints for our application

	- We separate the routes such that "index.js" file only contains information on the server

*/

const express = require("express");
// THis will allows us to use our routes in application
// allow us to access the http method
const router = express.Router();


// To import the COntrollers
// to use it we need to "Store the path of controllers file to a variable"
// Allow us to use the contents of the "taskController.js" file in the models folder
const taskController = require("../controllers/taskController");

/* Creating a Routes */

// Route to get all the tasks
router.get("/", (req, res) => {

	//fileName.functionNAmeOfFIle() from Controllers 
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

});

// Creating a new Document
router.post("/", (req, res) => {

	// to see what's the content of req.body
	console.log(req.body);

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));

});


// Deleting a document
router.delete("/:id", (req, res) => {
	console.log(req.params.id)

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// Updating a Document
// Route to update a task
router.put("/:id", (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

});

/********** Activity Start **********/
	/*
		Instructions:
			- Create a ROUTE for "getting a Specific task".
			- Create a CONTROLLER function for "Retrieving a Specific task".
			- Return the result back to the client/postman.
			- Process a GET request at the "/tasks/:id" endpoint route using postman to "Get a specific task".
			- Create a route for "Changing the status to COMPLETE".
			- Create a CONTROLLER function for "changing the status of a task to complete".
			- Return the result back to the client/postman.
			Process a PUT request at the "/tasks/:id/complete" endpoint route using postman to "update a task".
	*/
			// Route Updating Status from Pending to Complete
			router.put("/:id/complete", (req, res) => {

				console.log(req.params.id);
				console.log(req.body);

				taskController.updateStatusTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));

			});

			// Route for Retrieving a Specific Task
			router.get("/:id", (req, res) => {

				console.log(req.params.id);

				taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));

			});
/********** Activity End **********/


// Use "module.exports" to export the router object to use in the index.js
module.exports = router;